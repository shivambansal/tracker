from cleaner import Cleaner
import string 
punc = string.punctuation

class DimensionClassifier:
	def __init__(self):
		self.dimensionKeywordFilePath = "dep/NewDimDict.txt"
		self.noisyKeywords = "dep/noisekws.txt"

	def puncFree(self, txt, ignore):
		for each in punc:
			if each not in ignore:
				txt = txt.replace(each, "")
		return txt

	def clean_text(self, text):
		return Cleaner.clean(text)

	def get_entitiy_used(self, comment_text, entity):
		hashtags = []
		for word in comment_text.split():
			if word.startswith(entity):
				hashtags.append(word)
		return hashtags

	def get_dimensions(self, text):
		
		### Read Noisy Keywords
		noisyFile = open(self.noisyKeywords).read().strip().split("\n")
		noisyKeywords = []
		for x in noisyFile:
			noisyKeywords.extend(x.split(","))
		noisyKeywords = [x.strip() for x in noisyKeywords]

		### Check for tweets with noist
		spText = self.puncFree(text, ignore=['#', '@'])
		lemmatized = Cleaner.LemIt(text.lower())

		NoisyText = False
		for word in noisyKeywords:
			if word.startswith('"'):
				word = " " + word.lower() + " "
				if word.replace('"', "") in text:
					NoisyText = True
					break
				elif word.replace('"', "") in spText:
					NoisyText = True
					break
			else:
				word = " " + word.lower() + " "
				if word.replace('"', "") in text:
					NoisyText = True
					break
				elif word.replace('"', "") in lemmatized:
					NoisyText = True
					break
		if " | " in text:
			NoisyText = True

		# Read Dimensions
		dimensions = open(self.dimensionKeywordFilePath).read().strip().split("\n")
		dimensionDict = {}
		for line in dimensions:
			wrds = line.split("|")[1].split(",")
			wrds = [z.strip() for z in wrds]
			dimensionDict[line.split("|")[0].strip()] = wrds

		dimCollected = []
		if NoisyText == False:
			text = " " + text.lower().strip() + " "
			lemmatized = " " + lemmatized.lower().strip() + " "

			for key, value in dimensionDict.iteritems():
				for word in value:
					word = " " + word.lower().strip() + " "
					if word in text:
						dimCollected.append(key)
					elif word in lemmatized:
						dimCollected.append(key)
			dimCollected = list(set(dimCollected))

			if 'hope to win' in text or 'deal with' in text:
				if 'Offers' in dimCollected:
					dimCollected.remove('Offers')
			dimCollected = list(set(dimCollected))
		return dimCollected
	   

	def getKeywordsFromCSV(self, dimensionFilePath):
		dictParameter = {}
		with open(dimensionFilePath, 'rb') as csvfile:
			reader = csv.reader(csvfile)
			for row in reader:
				dictParameter[row[0]] = row[1].split("|")
		return dictParameter

	def classify_dimension(self, text):
		campaigns = self.get_entitiy_used(text, "#")

		cleaned = self.clean_text(text)
		dimensions = self.get_dimensions(cleaned)

		return dimensions 

DC = DimensionClassifier()

if __name__ == '__main__':
	text = "#BigBillionDay sale is on Month-Start & Sunday. Don't forget to manage your pockets for #OctoberMonthEnd #BigBillionDaysSneakPeek @Flipkart"
	text = "#ActOnMagic Is hybrid cloud hosting ideal for eCommerce campaign like #bigbillionday?. Read Blog: "
	text = "@Flipkart @flipkartsupport Order no. 507116795234951000 cancelled after shipping and you want us to order on #BigBillionDay @_sachinbansal"
	text = "Watch for #personalised #deals from Flipkart on Huew during #BigBillionDay #Huew Download at http://goo.gl/Gb2950 "
	text = "And @Flipkart says we only sell original products! #BigBillionDay #WhenAreBigBillionDays #Flipkart"
	text = "#WhenAreBigBillionDays? Remember The Last #BigBillionDay When #Flipkart Fooled Us By Increasing Prices Overnight And Giving Fake Discounts?"
	text = "#BigBillionDay: #Flipkart bets on exchange offers to drive sales"
	text = "Oops Something went wrong. #Nykaa just did an encore of that first memorable #BigBillionDay. What a colossal disappointment! @MyNykaa"
	text = "Site #giftcardsindia crashes due to overwhelming response, same feeling as #bigbillionday #flipkart, thanks to our patrons!"
	text = "#MumbaiUniversity admission process crashed similar to the Flipkart crash on the bigbillionday! Gone r physical servers! #Cloud LetsNurture"
	text = "HISTORY REPEATS ITSELF as my @MyntraSupport order#128697683 cancelled after MISLEADING me for 13DAYS assuring delivery. #BigBillionDay #EORS"
	text = "The #bigbillionday of #Flipkart ... Now we can kinda use the credit cards ...More like an extended version of #COD with add ons"
	text = "Nw u cn buy a brand new car on @Flipkart - #KUV100 will b available on d platform! #BigBillionDay applicable on tis too? :p @anandmahindra"
	print DC.classify_dimension(text)