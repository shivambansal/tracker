from esConnector import get , update
from DimensionClassifier import DC

input_query = {
	"size" : 1000,
    "query": {
        "bool": {
            "should": {
                "bool": {
                    "must_not": [{
                        "exists" : {
                            "field" : "dimension"
                        }
                    }],
                    "must": [
                        {
                            "terms": {
                                "cType": ["TWITTER", "INSTAGRAM", "FACEBOOK"]
                            } 
                        },
                        {
                            "terms": { 
                            		"hT": ['debate']
                            }
                        }
                    ]
                }
            },
            "minimum_should_match": "1"
        }
    }
}

_config = {
	'input_query' : input_query,
	'size' : 100,
	'scroll' : '2m',
}

documents = get._get_stream(_config)

bulk_body = ''
for document in documents:
    if "m" not in document:
        document['dimension'] = []
    else:
    	dimension = DC.get_dimensions(document['m'])
        document['dimension'] = dimension

    ### Single Update
    # update._update_document(dimension, document['id'], document['_index'])

    # doc = {
    #     '_id': result['_id'], 
    #     "_type": "the-type", 
    #     "_index": "the-index", 
    #     "_source": {'doc': result['_source']}, 
    #     '_op_type': 'update'
    #     }           

    bulk_body += '{ "update" : {"_id" : "'+document['id']+'", "_index" : "' + document['_index'] + '", "_type" : "propheseePost"} }\n'
    bulk_body += '{ "script": "ctx._source.dimension = ' + str(dimension) +'"}\n'
    print bulk_body

    if len(bulk_body.split("\n")) % 10 == 1:
        update._bulk_operation(bulk_body)
        bulk_body = ''